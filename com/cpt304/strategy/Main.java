package com.cpt304.strategy;

import com.cpt304.strategy.services.MessageService;
import com.cpt304.strategy.services.MessageStrategy;
import com.cpt304.strategy.services.WeChatService;
import com.cpt304.strategy.services.WhatsAppService;

public class Main{
    public static void main(String[] argv){
        MessageService messageService;

        messageService = new MessageService();
        messageService.setStrategy(new WhatsAppService());
        messageService.sendMessage("Meet me there!");
    }
}