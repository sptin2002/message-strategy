package com.cpt304.strategy.services;

public interface MessageStrategy {
    public void send(String msg);
}
