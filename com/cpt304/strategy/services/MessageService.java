package com.cpt304.strategy.services;

public class MessageService {
    private MessageStrategy messageStrategy;

    public void setStrategy(MessageStrategy messageStrategy){
        this.messageStrategy = messageStrategy;
    }

    public void sendMessage(String msg){
        if(messageStrategy != null)
            messageStrategy.send(msg);
    }
}
